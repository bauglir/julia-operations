# Julia

A GitLab CI process for managing [Julia](https://julialang.org) packages.

## Usage

Include the [process](#process) definition into a project's `/.gitlab-ci.yml`
file using

<!-- BEGIN_VERSIONED_TEMPLATE

```yml
include:
  - file: "/.gitlab/ci/process.yml"
    project: "ethima/julia"
    ref: "v__NEXT_SEMANTIC_RELEASE_VERSION__"
```

-->

```yml
include:
  - file: "/.gitlab/ci/process.yml"
    project: "ethima/julia"
    ref: "v5.0.0"
```

<!-- END_VERSIONED_TEMPLATE_REPLACEMENT -->

This process is based on the ethima/gitlab-ci> project which provides basic
functionality for managing projects on GitLab, e.g. release and dependency
management, linting of non-Julia files, etc. Follow the instructions in [the
_Usage_ section in its documentation][gitlab-ci-usage-url] to finalize
configuration of the enrollment in this process and/or to learn how to opt-out
of jobs enabled by it.

## [Process](/.gitlab/ci/process.yml)

The [`process`](/.gitlab/ci/process.yml) creates pipelines that include jobs
for generic software management tasks, e.g. as defined by and included through
the ethima/gitlab-ci> process, as well as those specific to Julia packages.

Which [jobs](#jobs) are included in pipelines created through the process
depends on how the process was triggered. The following table outlines which
jobs are included for which [workflow scenarios](#workflows). Note that _only
jobs_ defined by this project are included in this list.

| Job                                                                                 | Scenario                                                                                                                           |
| ----------------------------------------------------------------------------------- | ---------------------------------------------------------------------------------------------------------------------------------- |
| [`Auto Quality Assurance`](#auto-quality-assurance)                                 | `Merge Request`                                                                                                                    |
| [`Generate Cobertura Coverage Report`](#generate-cobertura-coverage-report)         | `Merge Request`                                                                                                                    |
| [`Generate HTML Coverage Report`](#generate-html-coverage-report)                   | `Maintenance Branch (Push)`, `Merge Request`, `Prerelease Branch (Push)`, `Prerelease`, `Primary Release Branch (Push)`, `Release` |
| [`Run Tests`](#run-tests)                                                           | `Maintenance Branch (Push)`, `Merge Request`, `Prerelease Branch (Push)`, `Prerelease`, `Primary Release Branch (Push)`, `Release` |
| [`Verify Code Formatting (JuliaFormatter)`](#verify-code-formatting-juliaformatter) | `Merge Request`                                                                                                                    |

Jobs that are disabled for a given workflow scenario can be enabled by setting
their corresponding `ENABLE_*` environment variable to `"true"` and vice versa.

## Jobs

### [`Auto Quality Assurance`](/.gitlab/ci/auto-quality-assurance.yml)

The
[`.gitlab/ci/auto-quality-assurance.yml`](/.gitlab/ci/auto-quality-assurance.yml)
file defines a CI job that performs "auto quality assurance for Julia packages"
using [the `Aqua` package][aqua-url]. This package can detect common errors
such as method ambiguities, undefined exports and even certain cases of type
piracy.

By default all checks defined by [`Aqua`][aqua-url] are run against the package
under test, _except_ [the "project extras"
check][aqua-project-extras-check-url]. This check is only enabled by default if
both a ["test target"][pkg-test-target-url] and a ["test
project"][pkg-test-project-url] are defined for the package under test. Whether
a specific check is enabled can be controlled by configuring environment
variables corresponding to [the keyword arguments to `Aqua`'s `test_all`
function][aqua-test_all-kwargs-url]. The names of these environment variables
are the uppercased variants of the keyword argument names prefixed with
`AQUA_ENABLE_*` and the values either `"true"` or `"false"`, e.g. to disable
the "method ambiguity" check, define the environment variable
`AQUA_ENABLE_AMBIGUITIES="false"`.

Inclusion of the job in a pipeline is controlled through the
`ENABLE_AUTO_QUALITY_ASSURANCE` environment variable.

The version of [`Aqua`][aqua-url] used to perform auto quality assurance is
controlled through the `AQUA_VERSION` environment variable.

### [`Generate Cobertura Coverage Report`](/.gitlab/ci/generate-cobertura-coverage-report.yml)

The
[`.gitlab/ci/generate-cobertura-coverage-report.yml`](/.gitlab/ci/generate-cobertura-coverage-report.yml)
file defines a CI job that generates a [Cobertura][cobertura-url]-compatible
coverage report from the [`lcov`][lcov-url]-formatted data provided by the `Run
Tests` job. These reports are for direct consumption, but power [GitLab's test
coverage visualization functionality for merge
requests][gitlab-test-coverage-visualization-url].

Inclusion of the job in a pipeline is controlled through the
`ENABLE_COBERTURA_COVERAGE_REPORT` environment variable. Furthermore, the
inclusion of the job in a pipeline is conditional on the `Run Tests` job being
included and code coverage collection being enabled.

### [`Generate HTML Coverage Report`](/.gitlab/ci/generate-html-coverage-report.yml)

The
[`.gitlab/ci/generate-html-coverage-report.yml`](/.gitlab/ci/generate-html-coverage-report.yml)
file defines a CI job that generates HTML reports using
[`genhtml`][genhtml-url] from the [`lcov`][lcov-url]-formatted data provided by
the `Run Tests` job. The reports are made available through GitLab's artifacts
system and can either be [downloaded locally][gitlab-artifacts-download-url] or
[viewed directly through GitLab][gitlab-artifacts-browser-url] from a variety
of locations.

Many aspects of the HTML reports can be customized through environment
variables, e.g. the header of the report, indentation of included code, etc.
See the job's definition for details.

Inclusion of the job in a pipeline is controlled through the
`ENABLE_HTML_COVERAGE_REPORT` environment variable. Furthermore, the inclusion
of the job in a pipeline is conditional on the `Run Tests` job being included
and code coverage collection being enabled.

### [`Run Tests`](/.gitlab/ci/run-tests.yml)

The [`.gitlab/ci/run-tests.yml`](/.gitlab/ci/run-tests.yml) file defines a CI
job that runs the test suite of a Julia package against a single Julia version.
The test suite is run using the [`TestReports`][test-reports-url] package. This
enables exporting the test results in [the JUnit XML format][junit-schema-url],
which powers [GitLab's unit test reporting
functionality][gitlab-unit-test-reporting-url]. The job captures the
`Manifest.toml`, as `Manifest-<Job ID>.toml`, produced during the instantiation
of the sandbox in which the tests are run for [easier debugging of test
failures](#verifying-a-test-run).

Inclusion of the job in a pipeline is controlled through the `ENABLE_RUN_TESTS`
environment variable. The job _requires_ a `test/runtests.jl` file and its
inclusion in a pipeline is conditional on it being present. It is therefore not
necessary to override the `ENABLE_RUN_TESTS` variable if no tests have been
defined.

The Julia version used to run the tests is controlled through the
`JULIA_VERSION` environment variable. Its value _must_ correspond to [a tag for
the `julia` image available on Docker Hub][dockerhub-julia-tag-url]. The
version of [`TestReports`][test-reports-url] used to run the tests is
controlled through the `TEST_REPORTS_VERSION` environment variable. In case the
package under test is not located at the root of the project, its path can be
specified through the `PACKAGE_PATH` environment variable. If specified this
value _must_ include a trailing slash!

#### Code Coverage

Code coverage can be gathered during the `Run Tests` job and is made available
to [GitLab's code coverage facilities][gitlab-code-coverage-url], e.g.
reporting in merge requests, CI job overviews, etc.

The `ENABLE_CODE_COVERAGE` environment variable controls whether code coverage
is gathered. This functionality is enabled by default for all scenarios in
which the `Run Tests` job runs.

The version of [`CoverageTools`][coverage-tools-url] used to process coverage
results is controlled through the `COVERAGE_TOOLS_VERSION` environment
variable.

#### Verifying a test run

The `Manifest-<Job ID>.toml` file produced by each test job can be used to
locally reproduce the job. The following steps can be used to locally
(re)produce the test results.

- Create a dedicated test folder, e.g. using `TEST_PATH="$(mktemp -d)"`.
- Download the `Manifest-<Job ID>.toml` artifact, from the artifacts of the
  failed job, into the test folder and rename it to `Manifest.toml`.
- `julia --project="${TEST_PATH}"`.
- `pkg> instantiate`.
- `julia> using TestReports`.
- `julia> TestReports.test("<Package Name>")`.

### [`Verify Code Formatting (JuliaFormatter)`](/.gitlab/ci/verify-code-formatting-julia-formatter.yml)

The
[`.gitlab/ci/verify-code-formatting-julia-formatter.yml`](/.gitlab/ci/verify-code-formatting-julia-formatter.yml)
file defines a CI job that verifies the formatting of Julia code in a package
using [`JuliaFormatter`][julia-formatter-url]. If any files are not
appropriately formatted the job fails. When the job fails, a `.patch` file with
the formatting corrections is attached to the job as an artifact. The
`JULIA_FORMATTER_VERBOSE_FAILURE` environment variable can be set to `"true"`
to also output the necessary changes in the logs of the job. By default this
variable is set to `"false"` to prevent leaking source code into logs.

[`JuliaFormatter`][julia-formatter-url] can be configured by adding a
[configuration file][julia-formatter-configuration-file-url] to a package's
folder. Alternatively, one of the style guides supported by `JuliaFormatter`
can be specified as the `JULIA_FORMATTER_STYLE` environment variable. Supported
values are [`"blue"`][julia-formatter-blue-style-url],
[`"sciml"`][julia-formatter-sciml-style-url],
[`"yas"`][julia-formatter-yas-style-url], where `"blue"` is the default. A
configuration file takes precedence over the `JULIA_FORMATTER_STYLE`
environment variable.

Inclusion of the job in a pipeline is controlled through the
`ENABLE_CODE_FORMATTING_JULIA_FORMATTER` environment variable.

The version of [`JuliaFormatter`][julia-formatter-url] used to verify
formatting of the Julia codebase is controlled through the
`JULIA_FORMATTER_VERSION` environment variable.

[aqua-project-extras-check-url]: https://juliatesting.github.io/Aqua.jl/stable/#Aqua.test_project_extras-Tuple{Any}
[aqua-test_all-kwargs-url]: https://juliatesting.github.io/Aqua.jl/stable/#Aqua.test_all-Tuple{Module}
[aqua-url]: https://juliatesting.github.io/Aqua.jl/stable/
[cobertura-url]: https://cobertura.github.io/cobertura
[coverage-tools-url]: https://github.com/JuliaCI/CoverageTools.jl
[dockerhub-julia-tag-url]: https://hub.docker.com/_/julia
[genhtml-url]: https://linux.die.net/man/1/genhtml
[gitlab-artifacts-browser-url]: https://docs.gitlab.com/ee/ci/jobs/job_artifacts.html#browse-the-contents-of-the-artifacts-archive
[gitlab-artifacts-download-url]: https://docs.gitlab.com/16.0/ee/ci/jobs/job_artifacts.html#download-job-artifacts
[gitlab-ci-usage-url]: https://gitlab.com/ethima/gitlab-ci#usage
[gitlab-code-coverage-url]: https://docs.gitlab.com/16.0/ee/ci/testing/code_coverage.html
[gitlab-test-coverage-visualization-url]: https://docs.gitlab.com/16.0/ee/ci/testing/test_coverage_visualization.html
[gitlab-unit-test-reporting-url]: https://docs.gitlab.com/16.0/ee/ci/testing/unit_test_reports.html
[julia-formatter-blue-style-url]: https://domluna.github.io/JuliaFormatter.jl/stable/blue_style/
[julia-formatter-configuration-file-url]: https://domluna.github.io/JuliaFormatter.jl/stable/config/
[julia-formatter-sciml-style-url]: https://domluna.github.io/JuliaFormatter.jl/stable/sciml_style/
[julia-formatter-url]: https://domluna.github.io/JuliaFormatter.jl/stable/
[julia-formatter-yas-style-url]: https://domluna.github.io/JuliaFormatter.jl/stable/yas_style/
[junit-schema-url]: https://www.ibm.com/docs/en/developer-for-zos/16.0?topic=formats-junit-xml-format
[lcov-url]: https://linux.die.net/man/1/lcov
[pkg-test-project-url]: https://pkgdocs.julialang.org/dev/creating-packages/#test/Project.toml-file-test-specific-dependencies
[pkg-test-target-url]: https://pkgdocs.julialang.org/dev/creating-packages/#target-based-test-specific-dependencies
[test-reports-url]: https://github.com/JuliaTesting/TestReports.jl
