# Changelog

# [5.0.0](https://gitlab.com/ethima/julia/compare/v4.0.0...v5.0.0) (2024-02-20)


### Build System

* **deps:** update dependency ethima/gitlab-ci to v10 ([671c2b6](https://gitlab.com/ethima/julia/commit/671c2b6804c7d613a312e2acf4dd8eba197e30ea))


### BREAKING CHANGES

* **deps:** The `ethima/semantic-release@8` process, which is
incorporated into this process through the ethima/gitlab-ci> project,
depends on `@ethima/semantic-release-configuration@8` and
`semantic-release@23` which in turn depend on `cosmiconfig@9`.
Primarily, this means that if a project is using a `config.js` file, or
an equivalent such as `config.json` or `config.ts`, to configure
`cosmiconfig` itself, the file needs to be moved into a `.config`
directory in the root of the project.

See `cosmiconfig`'s ["Usage for end users"
documentation][cosmiconfig-meta-config-url] for details and its [v9.0.0
release notes][cosmiconfig-v9-release-notes-url].

[cosmiconfig-meta-config-url]: https://github.com/cosmiconfig/cosmiconfig/blob/a5a842547c13392ebb89a485b9e56d9f37e3cbd3/README.md#usage-for-end-users
[cosmiconfig-v9-release-notes-url]: https://github.com/cosmiconfig/cosmiconfig/releases/tag/v9.0.0

# [4.0.0](https://gitlab.com/ethima/julia/compare/v3.0.0...v4.0.0) (2024-01-05)


* feat(deps)!: update dependency ethima/gitlab-ci to v9 ([90859ff](https://gitlab.com/ethima/julia/commit/90859ff99c29158317f93007c5046fcfe3abadff))


### BREAKING CHANGES

* The update to ethima/gitlab-ci@9 includes an update to
[@ethima/semantic-release@7](https://gitlab.com/ethima/semantic-release/-/releases/v7.0.0),
which in turn, includes a change to the commit type used for release
commits in the underlying configuration that is used. This type has been
changed from `chore` to `build`. This is only a breaking change for
those with additional workflows on top of the semantic release workflow
provided by this project relying on the structure of these commit
messages.

# [3.0.0](https://gitlab.com/ethima/julia/compare/v2.2.0...v3.0.0) (2023-10-30)

# [2.2.0](https://gitlab.com/ethima/julia/compare/v2.1.0...v2.2.0) (2023-10-27)


### Bug Fixes

* **testing:** enable colored output during testing ([e9f8fbe](https://gitlab.com/ethima/julia/commit/e9f8fbe44b6e682ec67572539937e835094ed4eb))


### Features

* **testing:** make the test environment's `Manifest.toml` available as an artifact ([d2d4fd2](https://gitlab.com/ethima/julia/commit/d2d4fd2a26c4815423e846e12b499b5ba94e384f))

# [2.1.0](https://gitlab.com/ethima/julia/compare/v2.0.0...v2.1.0) (2023-07-28)


### Bug Fixes

* **process:** remove the `Default Branch (Push)` scenario ([ca7e5fd](https://gitlab.com/ethima/julia/commit/ca7e5fdb3e6f1b04a6ba3f5fd2c3474622be2ec6))


### Features

* **process:** add the `Maintenance Branch (Push)` scenario from ethima/gitlab-ci> ([3166622](https://gitlab.com/ethima/julia/commit/31666226e9c358cfedc350f35e18bb622ea88ea9))
* **process:** add the `Prerelease Branch (Push)` scenario from ethima/gitlab-ci> ([ca7a26f](https://gitlab.com/ethima/julia/commit/ca7a26f932ce7d54f6e59222ce4c3af99f762bc2))
* **process:** add the `Prerelease` scenario from ethima/gitlab-ci> ([9634611](https://gitlab.com/ethima/julia/commit/963461100673200b874090ea421a64a239fa49a4))
* **process:** add the `Primary Release Branch (Push)` scenario from ethima/gitlab-ci> ([43c36b2](https://gitlab.com/ethima/julia/commit/43c36b237037a0ae531d58f8fe41884728959513))
* **process:** add the `Release` scenario from ethima/gitlab-ci> ([353c457](https://gitlab.com/ethima/julia/commit/353c45742e582b5f6c63a57f90e34c02fa774f10))

# [2.0.0](https://gitlab.com/ethima/julia/compare/v1.3.1...v2.0.0) (2023-07-28)

## [1.3.1](https://gitlab.com/ethima/julia/compare/v1.3.0...v1.3.1) (2023-06-12)


### Bug Fixes

* **quality-assurance:** add default `ENABLE_AUTO_QUALITY_ASSURANCE` variable ([df6d74e](https://gitlab.com/ethima/julia/commit/df6d74e19adbcd573803e564f7e0cf2ed885d612))

# [1.3.0](https://gitlab.com/ethima/julia/compare/v1.2.0...v1.3.0) (2023-06-11)


### Features

* add job to verify Julia code formatting ([51ee467](https://gitlab.com/ethima/julia/commit/51ee4670f8a701446e635d1ce081b6f7967771f4))
* **process:** verify Julia code formatting for merge requests ([1d88cc8](https://gitlab.com/ethima/julia/commit/1d88cc8795843d0ae5ed8cbe319cd586525f1b06))

# [1.2.0](https://gitlab.com/ethima/julia/compare/v1.1.0...v1.2.0) (2023-06-06)


### Features

* add job for "auto quality assurance" ([3b17707](https://gitlab.com/ethima/julia/commit/3b177073b116e6987e3e11016383e360872f8979))
* **process:** perform "auto quality assurance" for merge requests ([f6e6ec0](https://gitlab.com/ethima/julia/commit/f6e6ec09ae6f9129a99731790ba88acd601f6bf1))

# [1.1.0](https://gitlab.com/ethima/julia/compare/v1.0.0...v1.1.0) (2023-05-26)


### Features

* **process:** collect code coverage for merge requests ([8c2fc5b](https://gitlab.com/ethima/julia/commit/8c2fc5b3e0070639ce10eb56c669f00be0356f5d))
* **process:** collect code coverage for pushes to the default branch ([1817de5](https://gitlab.com/ethima/julia/commit/1817de5c6df3b2c140ffeada9ab75142daee6ed2))
* **process:** collect code coverage for releases ([d6214a8](https://gitlab.com/ethima/julia/commit/d6214a8cff7ed9e733363812bce2d685355d6d34))
* **process:** produce Cobertura code coverage reports for merge requests ([9f54059](https://gitlab.com/ethima/julia/commit/9f54059660a2fdcba405edcf3e31b9ac5886c445))
* **process:** produce HTML code coverage reports for merge requests ([d13b57d](https://gitlab.com/ethima/julia/commit/d13b57d92330e9a12d2e46ddaedf06ff9eaa87a4))
* **process:** produce HTML code coverage reports for pushes to the default branch ([772ea27](https://gitlab.com/ethima/julia/commit/772ea279c5420432145f9894e137ae045614667f))
* **process:** produce HTML code coverage reports for releases ([54620bc](https://gitlab.com/ethima/julia/commit/54620bc3c6c3deaa20615144e5e59b44989bff86))
* **process:** run package tests for merge requests ([5fbafe4](https://gitlab.com/ethima/julia/commit/5fbafe4ab5d8420d42c165249d20a4c54c943a2f))
* **process:** run package tests for pushes to the default branch ([2bf7272](https://gitlab.com/ethima/julia/commit/2bf72721cd1a19611ac1197381d405dd053b0ca0))
* **process:** run package tests for releases ([f56e335](https://gitlab.com/ethima/julia/commit/f56e3352cd1adf2610f11eb2763a0e5bf546abc5))
* **testing:** add `Generate Cobertura Coverage Report` job ([9db666d](https://gitlab.com/ethima/julia/commit/9db666d27bedbdb2df44d145d4dcae1955e82b33))
* **testing:** add `Generate HTML Coverage Report` job ([64c64a5](https://gitlab.com/ethima/julia/commit/64c64a5761616a690133e4433a1e1f8ce64eb3fc))
* **testing:** add `Run Tests` job reporting JUnit results ([f8f5edf](https://gitlab.com/ethima/julia/commit/f8f5edfefaa8920e6b942fc3c6fe7fb41ece1c29))
* **testing:** collect code coverage ([c18a980](https://gitlab.com/ethima/julia/commit/c18a9803424e55827ab6fd16b7b4cf783264063a))
* **testing:** expose aggregated coverage information as an artifact ([16638f8](https://gitlab.com/ethima/julia/commit/16638f8ee1727054335356ed57b6b5aa67473187))

# 1.0.0 (2023-05-06)


### Features

* **process:** include the ethima/gitlab-ci> process ([5f41d13](https://gitlab.com/ethima/julia/commit/5f41d132385bb4723923daa955797656d5f5b3bf))
